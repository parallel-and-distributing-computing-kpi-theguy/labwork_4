#include "Data.h"

/*------------------PaDC-----------------------------
---------------Labwork #4----------------------------
------------Threads in WinApi------------------------
--Group IO-52----------------------------------------
--Author: Butskiy Yuriy------------------------------
--Function 1 d = ((A*B) + (C*(B*(MA*MD))))-----------
--Function 2 MK = (TRANS(MA) * TRANS(MB * MM) + MX)--
--Function 3 T = (SORT(O + P) * TRANS(MR * MS)-------
--09.11.2017-----------------------------------------*/
int N;

void Matrix_Output(const Matrix &matrix)
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			cout << matrix[i][j] << "          ";
		}
		cout << endl;
	}
	cout << endl;
}

void Vector_Output(const Vector &vector)
{
	for (int i = 0; i < N; i++)
	{
		cout << vector[i] << "      ";
	}
	cout << endl;
}

void Matrix_Input(Matrix &matrix)
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			matrix[i][j] = 1;
		}
	}
}

void Vector_Input(Vector &vector)
{
	for (int i = 0; i < N; i++)
	{
		vector[i] = 1;
	}
}

Matrix Multiply_Matrixes(const Matrix &MA, const Matrix &MB)
{
	Matrix result(N, N);
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			int cell = 0;
			for (int k = 0; k < N; k++)
			{
				cell += MA[i][k] * MB[k][j];
			}
			result[i][j] = cell;
		}
	}
	return result;
}
Vector Multiply_Matr_Vect(const Matrix &MA, const Vector &A)
{
	Vector result(N);
	for (int i = 0; i < N; i++)
	{
		int cell = 0;
		for (int j = 0; j < N; j++)
		{
			cell += A[i] * MA[j][i];
		}
		result[i] = cell;
	}
	return result;
}
int Multiply_Vectors(const Vector &A, const Vector &B)
{
	int result = 0;
	for (int i = 0; i < N; i++)
	{
		result = 0;
		for (int j = 0; j < N; j++)
		{
			result += A[i] * B[j];
		}
	}
	return result;
}
Matrix Sum_Matrixes(const Matrix &MA, const Matrix &MB)
{
	Matrix result(N, N);
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			result[i][j] = MA[i][j] + MB[i][j];
		}
	}
	return result;
}
Vector Sum_Vectors(const Vector &A, const Vector &B)
{
	Vector result(N);
	for (int i = 0; i < N; i++)
	{
		result[i] = A[i] + B[i];
	}
	return result;
}
void Transpose_Matrix(Matrix MA)
{
	int cell;
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			cell = MA[j][i];
			MA[j][i] = MA[i][j];
			MA[i][j] = cell;
		}
	}
}
void Sort_Vector(Vector &A)
{
	for (int i = 0; i < N - 1; i++)
	{
		int min = i;
		for (int j = i + 1; j < N; j++)
		{
			if (A[min] > A[j])
				min = j;
		}
		int cell = A[i];
		A[i] = A[min];
		A[min] = cell;
	}
}

//Function 1 ((A*B) + (C*(B*(MA*MD))))
int F1(const Vector &A, const Vector &B, const Vector &C, const Matrix &MA, const Matrix &MD)
{
	int x = Multiply_Vectors(A, B);
	Matrix MK = Multiply_Matrixes(MA, MD);
	Vector K = Multiply_Matr_Vect(MK, B);
	int y = Multiply_Vectors(C, K);
	return (x + y);
}
//Function 2 (TRANS(MA) * TRANS(MB * MM) + MX)
Matrix F2(const Matrix &MA, const Matrix &MB, const Matrix &MM, const Matrix &MX)
{
	Matrix ME = MA;
	Transpose_Matrix(ME);
	Matrix MF = Multiply_Matrixes(MB, MM);
	Transpose_Matrix(MF);
	Matrix MR = Multiply_Matrixes(ME, MF);
	ME = Sum_Matrixes(MR, MX);
	return ME;
}
//Function 3 (SORT(O + P) * TRANS(MR * MS))
Vector F3(const Vector &O, const Vector &P, const Matrix &MR, const Matrix &MS)
{
	Vector A = Sum_Vectors(O, P);
	Sort_Vector(A);
	Matrix MB = Multiply_Matrixes(MR, MS);
	Transpose_Matrix(MB);
	Vector B = Multiply_Matr_Vect(MB, A);
	return B;
}