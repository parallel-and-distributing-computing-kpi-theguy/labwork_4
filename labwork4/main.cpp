#include <iostream>
#include "Data.h"
#include <Windows.h>

/*------------------PaDC-----------------------------
---------------Labwork #4----------------------------
------------Threads in WinApi------------------------
--Group IO-52----------------------------------------
--Author: Butskiy Yuriy------------------------------
--Function 1 d = ((A*B) + (C*(B*(MA*MD))))-----------
--Function 2 MK = (TRANS(MA) * TRANS(MB * MM) + MX)--
--Function 3 T = (SORT(O + P) * TRANS(MR * MS)-------
--09.11.2017-----------------------------------------*/

void T1()
{
	cout << "Task #1 started" << endl;
	Sleep(200);
	Vector A(N);
	Vector B(N);
	Vector C(N);
	Matrix MA(N, N);
	Matrix MD(N, N);
	Vector_Input(A);
	Vector_Input(B);
	Vector_Input(C);
	Matrix_Input(MA);
	Matrix_Input(MD);
	int d = F1(A, B, C, MA, MD);
	Sleep(1000);
	cout << "Task #1 finished" << endl;
	if (N < 10)
		cout << "T1: d = " << d << endl;
}

void T2()
{
	cout << "Task #2 started" << endl;
	Sleep(200);
	Matrix MA(N, N);
	Matrix MB(N, N);
	Matrix MM(N, N);
	Matrix MX(N, N);
	Matrix_Input(MA);
	Matrix_Input(MB);
	Matrix_Input(MM);
	Matrix_Input(MX);
	Matrix MK = F2(MA, MB, MM, MX);
	Sleep(2000);
	cout << "Task #2 finished" << endl;
	if (N < 10)
	{
		cout << "T2: MK = " << endl;
		Matrix_Output(MK);
	}
}

void T3()
{
	cout << "Task #3 started" << endl;
	Sleep(200);
	Vector O(N);
	Vector P(N);
	Matrix MR(N, N);
	Matrix MS(N, N);
	Vector_Input(O);
	Vector_Input(P);
	Matrix_Input(MR);
	Matrix_Input(MS);
	Vector T = F3(O, P, MR, MS);
	Sleep(3000);
	cout << "Task #3 finished" << endl;
	if (N < 10)
	{
		cout << "T3: T = " << endl;
		Vector_Output(T);
	}
}

int main()
{
	cout << "Enter the value N" << endl;
	cin >> N;

	DWORD threadID1, threadID2, threadID3;
	HANDLE th1, th2, th3;

	th1 = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)T1, NULL, CREATE_SUSPENDED, &threadID1);
	th2 = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)T2, NULL, CREATE_SUSPENDED, &threadID2);
	th3 = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)T3, NULL, CREATE_SUSPENDED, &threadID3);

	SetThreadPriority(th1, THREAD_PRIORITY_LOWEST);
	SetThreadPriority(th2, THREAD_PRIORITY_ABOVE_NORMAL);
	SetThreadPriority(th3, THREAD_PRIORITY_HIGHEST);

	ResumeThread(th1);
	ResumeThread(th2);
	ResumeThread(th3);

	WaitForSingleObject(th1, INFINITE);
	WaitForSingleObject(th2, INFINITE);
	WaitForSingleObject(th3, INFINITE);

	CloseHandle(th1);
	CloseHandle(th2);
	CloseHandle(th3);

	cout << "Labwork finished" << endl;
	system("pause");
	return 0;
}