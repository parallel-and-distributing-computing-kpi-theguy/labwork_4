/*------------------PaDC-----------------------------
---------------Labwork #4----------------------------
------------Threads in WinApi------------------------
--Group IO-52----------------------------------------
--Author: Butskiy Yuriy------------------------------
--Function 1 d = ((A*B) + (C*(B*(MA*MD))))-----------
--Function 2 MK = (TRANS(MA) * TRANS(MB * MM) + MX)--
--Function 3 T = (SORT(O + P) * TRANS(MR * MS)-------
--09.11.2017-----------------------------------------*/
#pragma once

#include <iostream>

extern int N;

using namespace std;

class Vector
{
	int n;
	int* internal;

public:
	Vector(int n)
	{
		this->n = n;
		internal = new int[n];
	}

	Vector(const Vector &other)
	{
		this->n = other.n;
		internal = new int[n];
		memcpy(internal, other.internal, n * sizeof(int));
	}

	Vector& operator=(const Vector &other)
	{
		if (n != other.n)
		{
			delete[] internal;
			n = other.n;
			internal = new int[n];
		}
		memcpy(internal, other.internal, n * sizeof(int));
		return *this;
	}

	~Vector()
	{
		delete[] internal;
	}

	int operator[](int i) const
	{
		return internal[i];
	}

	int& operator[](int i)
	{
		return internal[i];
	}
};

class Matrix
{
	int m, n;
	int* internal;

public:
	Matrix(int m, int n)
	{
		this->m = m;
		this->n = n;
		internal = new int[m * n];
	}

	Matrix(const Matrix &other)
	{

		this->m = other.m;
		this->n = other.n;
		internal = new int[m * n];
		memcpy(internal, other.internal, m*n * sizeof(int));
	}

	Matrix& operator=(const Matrix &other)
	{
		if (m != other.m && n != other.n)
		{
			delete[] internal;
			m = other.m;
			n = other.n;
			internal = new int[m * n];
		}
		memcpy(internal, other.internal, m*n * sizeof(int));
		return *this;
	}

	~Matrix()
	{
		delete[] internal;
	}

	int* operator[](int i) const
	{
		return &internal[i*n];
	}
};

//Input functions
void Matrix_Input(Matrix &matrix);
void Vector_Input(Vector &vector);

//Output functions
void Matrix_Output(const Matrix &matrix);
void Vector_Output(const Vector &vector);

//Function 1 ((A*B) + (C*(B*(MA*MD))))
int F1(const Vector &A, const Vector &B, const Vector &C, const Matrix &MA, const Matrix &MD);
//Function 2 (TRANS(MA) * TRANS(MB * MM) + MX)
Matrix F2(const Matrix &MA, const Matrix &MB, const Matrix &MM, const Matrix &MX);
//Function 3 (SORT(O + P) * TRANS(MR * MS))
Vector F3(const Vector &O, const Vector &P, const Matrix &MR, const Matrix &MS);